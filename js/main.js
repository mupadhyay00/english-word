function permutation(str) {
  var arr = str.split('');
  var permutations = [];
  var next, restPerms = [], picked, rest;

  if(arr.length === 0) return [str];
  for(var i = 0; i < arr.length; i++ ) {
    rest = Object.create(arr);
    picked = rest.splice(i, 1);

    restPerms = permutation(rest.join(''));
    for(var j = 0, jLen = restPerms.length; j < jLen; j++) {
      next = picked.concat(restPerms[j]);
      permutations.push(next.join(''))
    }
  }
  return permutations;
}
function possibleWords(str) {
  console.log(str);
  if(str) {
    const arr = permutation(str);
    const uniqueArray = arr.filter(function(item, pos){
      return arr.indexOf(item) == pos
    });
    const html = uniqueArray.map(function(d) {
      return `<li><span class="yellow-circle"></span><span>${d}</span></li>`
    }).join('');
    document.querySelector('.word-lists').innerHTML = html;
  }
}
const myinput = document.querySelector('.taking-input');
myinput.addEventListener('keyup',function (e) {
  possibleWords(e.target.value);
});
